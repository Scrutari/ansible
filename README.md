Installation de Scrutari sur un serveur sous Debian avec Ansible

Cette installation utilise la procédure du paquet tomcat-user de Debian


Pour l'installation, les scripts commençant par step*- doivent être appelés dans l'ordre par la commande suivante :

 ansible-playbook -v -u (user) -i _(inventory).ini step(script).yml

l'utilisateur à mettre dans le paramètre -u (user) dépend du script, ceux dont le nom termine par _root doivent être appelé par un compte d'administrateur, ceux finissant par _user doivent être appelé par l'utilisateur créé à l'étape 1.

Le fichier indiqué par -i _(inventory).ini comprend les paramètres nécessaires à l'installation. En particulier, le premier script step1-install_root.yml va créer un utilisateur qui va être celui de Tomcat.

inventory-example.ini donne un exemple des paramètres, le renommer avec un nom commençant par un souligné (_mon_serveur.ini par exemple), ils sont ignorés par Git et ne risquent donc pas de créer un conflit à l'occasion d'une mise à jour

Les paramètres sont les suivants :

- host_name: nom de domaine du serveur Scrutari
- admin_email: courriel de l'administrateur
- user: nom de l'utilisateur qui lancera l'instance de Tomcat (créé par le premier script d'installation)
- group: nom du groupe de l'utilisateur
- key_file: chemin d'un fichier sur l'ordinateur local (celui de la personne lançant le script) contenant une clé publique pour se connecter à l'utilisateur créé pour Tomcat
- tomcat_version: version de Tomcat
- java_opts_xmx: option Java sur la mémoire maximale
- java_opts_autobox: option Java sur le nombre d'objets Integer prédéfinis
- scrutari_context: nom du contexte Tomcat 
- nginx_template: fichier dans le répertoire templates à utiliser comme modèle pour la configuration de nginx
- local_webinf_dir: chemin sur l'ordinateur local du répertoire WEB-INF/ comprenant la version de Scrutari à installer

À noter que la configuration du moteur Scrutari se fera dans le répertoire /home/(user)/scrutari-conf, les données seront enregistrées dans "/var/local/(user)/scrutari-var. Les différents chemins sont décrits dans vars/paths.yml.

Le script update_webinf_root est à utiliser pour mettre à jour la version de Scrutari.
